package com.eManas.eManasCore_ComponentsService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EManasCoreComponentsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EManasCoreComponentsServiceApplication.class, args);
	}

}
