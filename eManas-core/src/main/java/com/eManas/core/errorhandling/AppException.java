/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eManas.core.errorhandling;

import java.io.Serializable;

import lombok.Data;

@Data
public class AppException  extends Exception implements Serializable {
	private static final long serialVersionUID = -8999932578270387947L;
	
	/** 
	 * contains redundantly the HTTP status of the response sent back to the client in case of error, so that
	 * the developer does not have to look into the response headers. If null a default 
	 */
	int status;
	
	/** application specific error code */
	int code; 
		
	/** link documenting the exception */	
	String link;
	
	/** detailed error description for developers*/
	String location;	
	
	
	public AppException(int status, int code, String message,
			String location, String link) {
		super(message);
		this.status = status;
		this.code = code;
		this.location = location;
		this.link = link;
	}

	public AppException() { }
	

}
