package com.eManas.core.errorhandling;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ext.Provider;
import org.apache.commons.beanutils.BeanUtils;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Data;





@Provider
@XmlRootElement
@Data
public class ErrorMessage implements Serializable{
	        
	/** message describing the error*/
	@XmlElement(name = "message")
	String message;
        
	/** contains the same HTTP Status code returned by the server */
	@XmlElement(name = "status")
	int status;
	
	/** application specific error code */
	@XmlElement(name = "code")
	int code;
		
	/** link point to page where the error message is documented */
	@XmlElement(name = "link")
	String link;
	
	/** extra information that might useful for developers */
	@XmlElement(name = "location")
	String location;	

	
	public ErrorMessage(AppException ex){
		try {
			BeanUtils.copyProperties(this, ex);
		} catch (IllegalAccessException ex1) {
                    Logger.getLogger(ErrorMessage.class.getName()).log(Level.SEVERE, null, ex1);
		} catch (InvocationTargetException ex1) {
                    Logger.getLogger(ErrorMessage.class.getName()).log(Level.SEVERE, null, ex1);
            }
	}
	
	
}
