package com.eManas.core.errorhandling;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Provider
public class AppExceptionMapper implements ExceptionMapper<AppException> {
    protected static Log log = LogFactory.getLog(AppExceptionMapper.class);
//        private HttpHeaders headers;
//        //headers.getMediaType()
        @Override
	public Response toResponse(AppException ex) {
//        log.info("In AppExceptionMapper "  );
		return Response.status(ex.getStatus())
				.entity(new ErrorMessage(ex))
				.type(MediaType.APPLICATION_JSON).
				build();
	}

}
