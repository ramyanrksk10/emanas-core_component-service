package com.eManas.core.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.Response;
import static net.bull.javamelody.internal.common.Parameters.getServletContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;

import com.eManas.core.errorhandling.AppException;
import com.eManas.core.messages.AppConstants;
import com.eManas.core.utils.KeyConstants;

import jakarta.servlet.ServletContext;


public class FileConfigLoader{

    private Log log = LogFactory.getLog(getClass());
//    private static Properties prop = null;
    private static Map<String, Properties> configProp = new HashMap<>();
   
    public void loadProperties(String propsFile) throws AppException {
          StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(KeyConstants.PASSWORD_ENCRYPTION);
        encryptor.setAlgorithm("PBEWithMD5AndDES");
        Properties prop = new EncryptableProperties(encryptor);
        try {

            InputStream inputStream = null;
            if(!propsFile.isEmpty() && (propsFile.startsWith("file:"))){
                //remove file:
                propsFile.trim();
                String propsFileStr = propsFile.substring(5);
                log.info("Initializing Propertyfile(Absolute path) : " + propsFile);
                //Absolute path (catalina home)
                inputStream = new FileInputStream(propsFileStr);
            } else if(!propsFile.isEmpty() && (propsFile.startsWith("classpath:"))){

                //relative path (from classpath)
                propsFile.trim();
                String propsFileStr = propsFile.substring(10);
                log.info("Initializing Propertyfile(classpath) : " + propsFileStr);
                log.info("Classloader path : " + getClass().getClassLoader().getResource("") );
                if( getClass().getClassLoader().getResource(propsFileStr) != null) {
                    File file = new File(getClass().getClassLoader().getResource(propsFileStr).toURI());
                    inputStream = new FileInputStream(file);
                }
            } else {
                log.error("File path format incorrect. Please start with \"file:\" for absolute path or \"classpath:\" for relative path (wrt classpath) : " + propsFile);
                throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(),
                            AppConstants.APP_ERROR_CODE_400,
                            "File path format incorrect. Please start with file: for absolute path or classpath: for relative pat (wrt classpath)",
                            "Configuration",
                            AppConstants.EMANAS_SUPPORT_URL);
                
            }
               
            if (inputStream != null) {
                prop.load(inputStream);
                configProp.put(propsFile, prop);
            } else {
                //throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
                log.error("Sorry, unable to find " + propsFile);
                return;
            }            
        } catch (IOException ex) {
            log.error("Sorry, File not found : " + propsFile);
            throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(),
                        AppConstants.APP_ERROR_CODE_400,
                        "file doesnt exist",
                        "Configuration",
                        AppConstants.EMANAS_SUPPORT_URL);
           
        } catch (URISyntaxException ex) {
            log.error("Sorry, File URL not found : " + propsFile);
            throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(),
                        AppConstants.APP_ERROR_CODE_400,
                        "file URL does not  exist",
                        "Configuration",
                        AppConstants.EMANAS_SUPPORT_URL);

            }
            
        }

   public Properties getProperties(String propsFile) throws AppException {
        if(configProp.get(propsFile) == null ) {
            log.info("Propertyfile not initialized. calling init " + propsFile);
            loadProperties(propsFile);
        }
        return configProp.get(propsFile);
    }
    
    public void setProperties(String property, String value, String propsFile) {
        if(configProp.get(propsFile) != null ) {
            log.error("Propertyfile initialized already");
            configProp.get(propsFile).setProperty(property, value);
        } else {
            log.error("Propertyfile not initialized");
        }
//        configProp.get(propsFile).setProperty(property, value);
    }
    
    public void storeProperties(String propsFile) throws AppException {
        
        if(configProp.get(propsFile) == null ) {
            log.info("Propertyfile not initialized. calling init " + propsFile);
            loadProperties(propsFile);
        }     
        
        try {
            FileOutputStream outputStream = null;
           if(!propsFile.isEmpty() && (propsFile.startsWith("file:"))){
                //remove file:
                propsFile.trim();
                String propsFileStr = propsFile.substring(5);
                //Absolute path (catalina home)
                outputStream = new FileOutputStream(propsFileStr);
            } else if(!propsFile.isEmpty() && (propsFile.startsWith("classpath:"))){

                //relative path (from classpath)
                propsFile.trim();
                String propsFileStr = propsFile.substring(10);
                //relative path (from classpath)
               ServletContext context = getServletContext();
                outputStream = new FileOutputStream(new File(getClass().getClassLoader().getResource(propsFileStr).toURI()));
            } else {
                log.error("File path format incorrect. Please start with \"file:\" for absolute path or \"classpath:\" for relative path (wrt classpath) : " + propsFile);
                throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(),
                            AppConstants.APP_ERROR_CODE_400,
                            "File path format incorrect. Please start with file: for absolute path or classpath: for relative pat (wrt classpath)",
                            "Configuration",
                            AppConstants.EMANAS_SUPPORT_URL);
                
            }
            configProp.get(propsFile).store(outputStream, null);
//            configProp.get(propsFile).store(new FileOutputStream(propsFile), null);
//            prop.store(new FileOutputStream(new File(getClass().getClassLoader().getResource(propsFile).toURI())), null);
        } catch (IOException ex) {
            log.error("Not able to write Propertyfile " + propsFile);
        } catch (URISyntaxException ex) {
            log.error("Not able to write Propertyfile " + ex.getMessage());
        }
    }    
    /**
     * Reset the read properties so that the next try to read a property ready the file again
     */
    public void resetProperties() {
        log.info("Reset properties!");
        configProp = null;
    }    
}
