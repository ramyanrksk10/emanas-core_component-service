package com.eManas.core.configuration;

import lombok.Data;

@Data
public class ADMINConfigCore {
	
	 private String ndhmUser;
	    private String ndhmPass;
	    private String ndhmOrg;
	    private String ndhmTenant;
	    private String aaaBaseURL;
	    private String vifBaseURL;
	    private String taskManagerBaseURL;
	    private String allTaskMaxRecords;
	    private String readTimeout;
	    private String connectTimeout;
	    private String maxTotal;
	    private String DefaultMaxPerRoute;
	    private String dgsPassword;
	    private String dgsUserName;
	    private String dgsCheckDuplicateFields;
	    private String dgsDuplicateCheckFields;
	    private String dgsBaseURL;
	    private String dgsDuplicateCheckFieldsOrg;
	    private String maxDuplicateObjectsOrg;
	    private String maxDuplicateObjects;
	    private String maxCheckDuplicateObjects;
	    private String predefinedNames;
	    private String noteBaseURL;
	    private String ethercisUserName;
	    private String ethercisPassword;            
	    private String ethercisBaseURL;
	    private String ethercisServiceURL;
	    private String ehrBaseUserName;
	    private String ehrBasePassword;            
	    private String ehrBaseBaseURL;
	    private String ehrServerMode;
	    private String ehrBaseEhrJson;

	    private String auditBaseURL;
	    private String auditUserName;
	    private String auditPassword;
	    private String supportServiceURL;
	    private String shreyasQueryDurationinDays;            
	    private String tenantID;

}
