package com.eManas.core.configuration;

import lombok.Data;

@Data
public class AAAConfigCore {
	
	private String loginPasswordHashStrength; 
    private String loginDynamicPasswordHashStrength; 
    private String loginDynamicSaltTimeout;
    private String loginSessionTimeout;
    private String PublicKeyFilePath;
    private String PrivateKeyFilePath;
    private String validateUserPassword;
    private String minimumUserPasswordLength;
    private String maximumUserPasswordLength;
    private String userPasswordContainUppercase;
    private String userPasswordContainLowercase;
    private String userPasswordContainDigit;
    private String userPasswordContainSpecialChar;
    private String userPasswordPolicyJson;
    private String aaaPassword;
    private String rateControllerEnabled;
    private String maxRateControllerFailures;
    private String rateControllerMonitorTimeInterval;
    private String rateControllerDisableTimeInterval;
    private String forgotPasswordURL;
    private String supportEmail;
    private String supportPassword;
    private String hostURL;
    private String portNumber;
    private String smtpAuth;
    private String starttlsEnable;
    private String sslEnable;
    private String requestEmailBody;
    private String successEmailBody;
    private String failureEmailBody;

}
