
 package com.eManas.core.utils;


public class Constants {
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    
    public static final String SESSIONID_PREFIX = "SessionId:";
    public static final String USER_NAME = "UserName";
    public static final String ORG_ID = "OrgId";
    public static final String TOKEN = "Token";
    public static final String SESSION_DATA = "SESSION_DATA";
    public static final String DEFAULT_LOGIN_ORG = "SELF";
    public static final String HL_AUDIT="Audit_Log";
    public static final String DEFAULT_TENANT_ID = "tenantID";
 
    //config
    public static final String DEFAULT_PROPERTY_NAME = "config.properties";
    public static final String FORGOT_PASSWORD_URL = "forgotPasswordURL";
    public static final String REQUEST_EMAIL_BODY = "requestEmailBody";    
    public static final String SUCCESS_EMAIL_BODY = "successEmailBody";    
    public static final String FAILURE_EMAIL_BODY = "failureEmailBody";       
    public static final String SUPPORT_EMAIL = "supportEmail";
    public static final String SUPPORT_PWD = "supportPassword";
    public static final String HOST_URL = "hostURL";
    public static final String PORT_NO = "portNumber";
    public static final String STARTTLS_ENABLE = "starttlsEnable";
    public static final String SSL_ENABLE = "sslEnable";
    public static final String SMPT_AUTH = "smtpAuth";
    public static final String LOGIN_SESSION_TIMEOUT = "loginSessionTimeout";
   
    public static final String AAA_BASEURL_PROPERTY = "aaaBaseURL"; 
    public static final String AUDIT_BASEURL_PROPERTY = "auditBaseURL";
    public static final String VIF_BASEURL_PROPERTY = "vifBaseURL";
    public static final String NOTE_BASEURL_PROPERTY = "noteBaseURL";
    public static final String TASK_MANAGER_BASEURL_PROPERTY = "taskManagerBaseURL";
    public static final String GET_ALL_TASK_MAXRECORDS_PROPERTY = "allTaskMaxRecords";
    public static final String DEMOGRAPHIC_BASEURL_PROPERTY = "dgsBaseURL";
    public static final String DUPLICATE_CHECK_FIELDS = "dgsDuplicateCheckFields";
    public static final String MAX_DUPLICATE_OBJECTS = "maxDuplicateObjects";
    public static final String CHECK_DUPLICATE_FIELDS = "dgsCheckDuplicateFields";
    public static final String MAX_CHECK_DUPLICATE_OBJECTS = "maxCheckDuplicateObjects";
    public static final String EHR_BASEURL_PROPERTY = "ethercisBaseURL";
    public static final String EHR_SERVICE_URL_PROPERTY = "ethercisServiceURL";
    public static final String SUPPORT_SERVICE_BASEURL_PROPERTY = "supportServiceURL";
    public static final String LOG_PROPERTY = "log";
    public static final String LOG_MODULE_PROPERTY = "logModule";
    public static final String LOG_LEVEL_PROPERTY = "logLevel";
    public static final String DUPLICATE_CHECK_FIELDS_ORG = "dgsDuplicateCheckFieldsOrg";
    public static final String MAX_DUPLICATE_OBJECTS_ORG = "maxDuplicateObjectsOrg";
    public static final String SHREYAS_GUEST_QUERY_DURATION_IN_DAYS = "shreyasQueryDurationinDays";
    public static final String ETHERCIS_USERNAME = "ethercisUserName";
    public static final String ETHERCIS_PASSWORD = "ethercisPassword";
    public static final String DGS_USERNAME = "dgsUserName";
    public static final String DGS_PASSWORD = "dgsPassword";
    public static final String AUDIT_USERNAME = "auditUserName";
    public static final String AUDIT_PASSWORD = "auditPassword";
    public static final String PRE_DEFINED_NAMES ="predefinedNames";
    public static final int SAME_ORG = 1;       //0001
    public static final int PRIM_PARENT_ORG = 2;//0010
    public static final int SEC_PARENT_ORG = 4; //0100
    public static final int CHILD_ORG = 8;      //1000   
    public static final int ENTIRE_NETWORK = 15; //1111
    public static final String AAA_PASSWORD = "aaaPassword";
    public static final String DGS_SHIRO_PASSWORD = "dgsShiroPassword";
    
    //userPassword validation
    public static final String VALIDATE_USER_PASSWORD = "validateUserPassword";
    public static final String MINIMUM_USER_PASSWORD_LENTH = "minimumUserPasswordLength";
    public static final String MAXIMUM_USER_PASSWORD_LENTH = "maximumUserPasswordLength";
    public static final String USER_PASSWORD_CONTAIN_UPPERCASE = "userPasswordContainUppercase";
    public static final String USER_PASSWORD_CONTAIN_LOWERCASE = "userPasswordContainLowercase";
    public static final String USER_PASSWORD_CONTAIN_DIGIT = "userPasswordContainDigit";
    public static final String USER_PASSWORD_CONTAIN_SPECIAL_CHAR = "userPasswordContainSpecialChar";
    public static final String USER_PASSWORD_POLICY_JSON = "userPasswordPolicyJson";
   
    //loginPasswordHash and Encrypt config
    public static final int DEFAULT_PASSWORD_HASH_STRENGTH = 12;
    public static final String LOGIN_PASSWORD_HASH_STRENGTH = "loginPasswordHashStrength";
    public static final String LOGIN_DYNAMICSALT_TIMEOUT = "loginDynamicSaltTimeout";
    public static final String KEY_PAIR_ENCRYPTION_PUBLIC_KEY_PATH = "PublicKeyFilePath";
    public static final String KEY_PAIR_ENCRYPTION_PRIVATE_KEY_PATH = "PrivateKeyFilePath";
    
    public static final String RATE_CONTROLLER_FAILURES_ENABLED = "rateControllerEnabled";
    public static final String MAX_RATE_CONTROLLER_FAILURES = "maxRateControllerFailures";
    public static final String RATE_CONTROLLER_MONITOR_TIME_INTERVAL = "rateControllerMonitorTimeInterval";
    public static final String RATE_CONTROLLER_DISABLE_TIME_INTERVAL = "rateControllerDisableTimeInterval";
    
    public static final String GEN_SALT_RESOURCE = "genSalt";
    public static final String FORGOT_PASSWORD_RESOURCE = "forgotPassword";
    public static final String UPDATE_PASSWORD_RESOURCE = "updatePassword";
    public static final String RESET_PASSWORD_RESOURCE = "resetPassword";
    public static final String FAILED_LOGIN_RESOURCE = "failedLoginResource";
    
}
