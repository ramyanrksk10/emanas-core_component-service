/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eManas.core.logging;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class AccessLog {

        static Log logger = LogFactory.getLog(AccessLog.class);
	
	public static void log(String user,String method,String path){
        
		logger.info(user + ":" +method +":" + path);
	} 
	
	public static void info(String msg)
	{
		logger.info(msg);
	}

}
