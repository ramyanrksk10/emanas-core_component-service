/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eManas.core.logging;

import lombok.Data;

@Data
public class LogParse {
   
    private String UserName;
    private String Method;
    private String Path;

    public LogParse(String UserName, String Method, String Path)
    {
        this.UserName = UserName;
        this.Method = Method;
        this.Path = Path;
    }

    

    public String toString()
    {
        return "UserName: " + UserName + "\n" + "Method: " + Method + "\n"
                    + "Path: " + Path;
    }

    
}
