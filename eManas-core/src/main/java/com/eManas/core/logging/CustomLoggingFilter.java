package com.eManas.core.logging;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.message.internal.ReaderWriter;

import com.eManas.core.errorhandling.AppException;
import com.eManas.core.messages.AppConstants;
import com.eManas.core.session.SessionData;
import com.eManas.core.utils.Constants;
import com.google.common.base.Stopwatch;

import java.util.concurrent.TimeUnit;


@Provider

//@Logged
public class CustomLoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

    @Context
    private ResourceInfo resourceInfo;


    static Log log = LogFactory.getLog(CustomLoggingFilter.class);
    
    private static ThreadLocal<Stopwatch> sw = new ThreadLocal<Stopwatch>();
    

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Stopwatch sw1 = sw.get();
        if (sw1 == null ) {
            sw.set( Stopwatch.createUnstarted());
        }
        sw.get().reset();
        sw.get().start();

        logRequestHeader(requestContext);



    }

    private void logRequestHeader(ContainerRequestContext requestContext) {

            StringBuilder sb = new StringBuilder();
            SessionData sessionData = new SessionData();

            String path = requestContext.getUriInfo().getPath();
            if(path.contains("login")) {
                  return;
            }

            String token = requestContext.getHeaders().getFirst("authorization");
            if(token == null ){
                sb.append(" RestAPI: \'").append(requestContext.getUriInfo().getAbsolutePath()).append("\'");
                sb.append(", HttpMethod: \'").append(requestContext.getMethod()).append("\'");
    
            } else {
            try {
                decodeSessionId(token, sessionData); 

                sb.append(" RestAPI: \'").append(requestContext.getUriInfo().getAbsolutePath()).append("\'");
                sb.append(", HttpMethod: \'").append(requestContext.getMethod()).append("\'");
                sb.append(", User: \'").append(sessionData.getUserName() == null ? "unknown\'"
                        : sessionData.getUserName() + "\'");
                sb.append(", Organization: \'").append(sessionData.getOrgId()).append("\'");
                sb.append(", Tenant: \'").append(sessionData.getTenantId()).append("\'");
                sb.append(", Role: \'").append(sessionData.getRole()).append("\'");
                sb.append(", AdminToken: \'").append(sessionData.getToken()).append("\'");

            } catch (AppException ex) {
                sb.append("No Token in the API ");
                sb.append(" RestAPI: \'").append(requestContext.getUriInfo().getAbsolutePath()).append("\'");
                sb.append(", HttpMethod: \'").append(requestContext.getMethod()).append("\'");

            }
        }
        log.info(Constants.HL_AUDIT + " Request: " + sb.toString() + "\n");
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)  throws IOException {
            logRequestHeader(requestContext, responseContext);

    }

    private void logQueryParameters(ContainerRequestContext requestContext) {
        Iterator<String> iterator = requestContext.getUriInfo().getPathParameters().keySet().iterator();
        while (iterator.hasNext()) {
            String name = iterator.next();
            List<String> obj = requestContext.getUriInfo().getPathParameters().get(name);
            String value = null;
            if (null != obj && obj.size() > 0) {
                value = obj.get(0);
            }
              log.debug("Query Parameter Name: {}, Value :{}" +  name + value);
        }
    }

    private void logMethodAnnotations() {
        Annotation[] annotations = resourceInfo.getResourceMethod().getDeclaredAnnotations();
        if (annotations != null && annotations.length > 0) {
           
            for (Annotation annotation : annotations) {
                   log.debug(annotation.toString());
            }
            
        }
    }

    private void logRequestHeader(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        StringBuilder sb = new StringBuilder();
        Stopwatch sw1 = sw.get();
        if (sw1 == null ) {
            sw.set( Stopwatch.createUnstarted());
            sw.get().start();
            sw1 = sw.get();
        }

        SessionData sessionData = new SessionData();
        String token = requestContext.getHeaders().getFirst("authorization");
        if(token == null ){
                sb.append(" RestAPI: \'").append(requestContext.getUriInfo().getAbsolutePath()).append("\'");
                sb.append(", HttpMethod: \'").append(requestContext.getMethod()).append("\'");
                sb.append(", HttpCode: ").append(responseContext.getStatus()).append("");
        }
        else {
            try {
                decodeSessionId(token, sessionData); 
                sb.append(" RestAPI: \'").append(requestContext.getUriInfo().getAbsolutePath()).append("\'");
                sb.append(", HttpMethod: \'").append(requestContext.getMethod()).append("\'");
                sb.append(", HttpCode: ").append(responseContext.getStatus()).append("");
                sb.append(", User: \'").append(sessionData.getUserName() == null ? "unknown\'"
                        : sessionData.getUserName() + "\'");
                sb.append(", Organization: \'").append(sessionData.getOrgId()).append("\'");
                sb.append(", Tenant: \'").append(sessionData.getTenantId()).append("\'");
                sb.append(", Role: \'").append(sessionData.getRole()).append("\'");
                sb.append(", AdminToken: \'").append(sessionData.getToken()).append("\'");
   
                       
            } catch (AppException ex) {

                sb.append(" No Token in the API ");
                sb.append(" RestAPI: \'").append(requestContext.getUriInfo().getAbsolutePath()).append("\'");
                sb.append(", HttpMethod: \'").append(requestContext.getMethod()).append("\'");
                sb.append(", HttpCode: ").append(responseContext.getStatus()).append("");
            }
        }
        log.info(Constants.HL_AUDIT + " Response:" + sb.toString() + "  ####$$$$#### Elapsed Time:" + sw1.elapsed(TimeUnit.MILLISECONDS) + " ms" + "\n");
    }

    private String readEntityStream(ContainerRequestContext requestContext) {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        final InputStream inputStream = requestContext.getEntityStream();
        final StringBuilder builder = new StringBuilder();
        try {
            ReaderWriter.writeTo(inputStream, outStream);
            byte[] requestEntity = outStream.toByteArray();
            if (requestEntity.length == 0) {
                builder.append("");
            } else {
                builder.append(new String(requestEntity));
            }
            requestContext.setEntityStream(new ByteArrayInputStream(requestEntity));
        } catch (IOException ex) {
            log.error("----Exception occurred while reading entity stream :{}" + ex.getMessage());
        }
        return builder.toString();
    }

    public boolean decodeSessionId(String token, SessionData sessionData) throws AppException {
        boolean flag = false;
        try {
            
            if ("".equals(token) || token == null) {
                String text = "Null Token: Please provide token in the Authorization header ";
               
                throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(),
                        AppConstants.APP_ERROR_CODE_400,
                        text,
                        "CustomLoggingFilter::decodeSessionId",
                        AppConstants.EMANAS_SUPPORT_URL);

            }
            String segments[] = token.split("#");
            String loginName = segments[1];
            if(loginName != null ){
                segments = loginName.split(":");
                if(segments[0] != null) {
                    sessionData.setUserName(segments[0]);
                    if(segments[1] != null) {
                       sessionData.setOrgId(segments[1]);
                        if(segments[2] != null) {
                           sessionData.setTenantId(segments[2]);
                            if(segments[3] != null) {
                               sessionData.setRole(segments[3]);
//                                if(segments[4] != null) {
//                                   sessionData.setProfession(segments[4]);
                                   sessionData.setToken(token);
                                   flag = true;
//                                }
                            }
                        }
                    }
                }
             }
            //Token segments are null
            if (!flag) {
                String text = "Can't decode token: " + token;
                throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(),
                        AppConstants.APP_ERROR_CODE_400,
                        text,
                        "CustomLoggingFilter::decodeSessionId",
                        AppConstants.EMANAS_SUPPORT_URL);
            }
        } catch (AppException e) {
            throw e;
        } catch (Exception e) {
            String text = "Can't decode token: " + token;
            throw new AppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    AppConstants.APP_ERROR_CODE_400,
                    text,
                    "CustomLoggingFilter::decodeSessionId",
                    AppConstants.EMANAS_SUPPORT_URL);
        }
         return true;
    }
}
