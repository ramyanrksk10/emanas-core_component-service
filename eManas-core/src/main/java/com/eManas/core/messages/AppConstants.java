
package com.eManas.core.messages;

public class AppConstants {
 	public static final int GENERIC_APP_ERROR_CODE = 5001;
        public static final int SERVER_NOT_FOUND = 404;
        public static final int APP_ERROR_CODE_400 = 400;
        public static final int APP_CONFLICT_409 = 409;
        public static final int APP_FORBIDDEN_403 = 403;
        public static final int APP_Unauthorized_401 = 401;
	public static final String EMANAS_SUPPORT_URL = "http://www.eManas.in/";
	public static final String FB_EMANAS_SUPPORT_URL = "http://www.eManas.com/";
   
}
