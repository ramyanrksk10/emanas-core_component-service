package com.eManas.core.session;

import lombok.Data;

@Data
public class SessionData {
	
	  private String userName;
      private String orgId;
      private String token;
      private String tenantId;
      private String role;
      private String profession;


}
